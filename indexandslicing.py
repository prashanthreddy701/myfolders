'''Slice with the [start:stop] syntax outputs elements in the interval [start, stop)
Slice with the [start:stop:step] syntax outputs elements in the interval [start, stop) with a step size of step'''
import numpy as np
numbers_array = np.array([1, 3, 10, 0, 2, 15, 7, 100])
sliced_arr = numbers_array[4:7] 
print("sliced array: ", sliced_arr)

numbers_array = np.array([1, 3, 10, 0, 2, 15, 7, 100])
sliced_arr = numbers_array[1:-2]
print("sliced array with -ve index:", sliced_arr)
#[start,stop,step]
numbers_array = np.array([1, 3, 10, 0, 2, 15, 7, 100])
sliced_arr = numbers_array[1:-2:2]
print("sliced array with step size & -ve index:", sliced_arr)

sliced_arr = numbers_array[:4]
print("sliced array with start index omitted:", sliced_arr)

sliced_arr = numbers_array[-3:]
print("sliced array with end index omitted:", sliced_arr)

numbers_array = np.array([1, 3, 10, 0, 2, 15, 7, 100])
slice_1 = numbers_array[1::2]
print("sliced array with step size & end index omitted:", slice_1)

slice_2 = numbers_array[::2]
print("sliced array with step size & both start and end index omitted:", slice_2)

#starting step will be from -1 hve to start from back
numbers_array = np.array([1, 3, 10, 0, 2, 15, 7, 100])
slice_1 = numbers_array[-1:0:-2]
print("sliced array with -ve step size:", slice_1)

slice_2 = numbers_array[3:-1:-1]
print("sliced array with -ve step size, stop to the right of start:", slice_2)

slice_3 = numbers_array[-2:1:2]
print("sliced array with +ve step size, stop to the left of start:", slice_3)
#from first row to till second row
array_2d = np.array([['A', 'B', 'C', 'D'],
                     ['F', 'G', 'H', 'I'],
                     ['K', 'L', 'M', 'N']])

print(array_2d[1:3])
print(array_2d[1:3, 2:4])
print(array_2d[1:, 2:])
#all rows and all columns and step size with 2 from left side to right side
array_2d = np.array([['A', 'B', 'C', 'D'],
                     ['F', 'G', 'H', 'I'],
                     ['K', 'L', 'M', 'N']])

print(array_2d[:, 0:4:2])
#rows from -1 to -4 and step size is for row selecting from bottom like so and all columns are selected
print(array_2d[-1:-4:-2, :])

array_2d = np.array([['A', 'B', 'C', 'D'],
                     ['F', 'G', 'H', 'I'],
                     ['K', 'L', 'M', 'N']])

sliced_array = array_2d[1:3, :]

sliced_array[1] = 'Z'
print("sliced_array after modifying:\n", sliced_array)
print("array_2d after modifying the slice:\n", array_2d)
array_2d = np.array([['A', 'B', 'C', 'D'],
                     ['F', 'G', 'H', 'I'],
                     ['K', 'L', 'M', 'N']])

selected_rows1 = array_2d[(0,2), (0,0)]
print("selected rows:", selected_rows1)
print("--")
print(array_2d[[1]][:,1:3])

selected_rows2 = array_2d[[0,2], 0]
print("selected rows alternative:", selected_rows2)

selected_rows3 = array_2d[[0,2]]
print("selected rows omitting columns: \n", selected_rows3)

         
